// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

const fs = require("fs");

function fixImage(obj, filed = "image") {
  if (obj == null) return obj;
  if (obj[filed].charAt(0) == "/")
    obj[filed] = __dirname + "/src/assets" + obj[filed];
  return obj;
}

module.exports = function(api) {
  api.loadSource(({ addCollection, addSchemaTypes, createReference }) => {
    addSchemaTypes(`
          type Sponsor{
            title: String,
            image: Image,
            desc: String,
            link: String
          }
          type Criterion {
            title: String,
            desc: String,
            icon: Image
          }
          type AgeData {
            from: Int,
            to: Int,
            theme: String,
            task: String,
            image: Image
          }
          type Nomination implements Node {
            title: String
            status: String,
            isNew: Boolean,
            isTech: Boolean,
            slug: String,
            icon: String,
            desc: String,
            sponsor: Sponsor,
            ageData: [AgeData],
            criterions: [Criterion],
            requirements: [Criterion]
          }`);

    let nominations = addCollection("Nomination");
    const nominationsRaw = JSON.parse(
      fs.readFileSync("./data/nominations.json")
    );

    for (const key in nominationsRaw) {
      const nomination = nominationsRaw[key];

      nominations.addNode({
        id: nomination.slug,
        title: nomination.ukrName,
        status: nomination.status,
        isNew: nomination.isNew,
        isTech: nomination.isTech,
        slug: nomination.slug,
        icon: nomination.icon,
        desc: nomination.desc,
        sponsor: fixImage(nomination.sponsor),
        ageData: nomination.age.map((ageData) => fixImage(ageData)),
        criterions: nomination.criterions.map((criterion) =>
          fixImage(criterion, "icon")
        ),
        requirements: nomination.requirements.map((requirement) =>
          fixImage(requirement, "icon")
        ),
      });
    }

    addSchemaTypes(`
        enum Position { 
          ALL
          JUDGE
          MENTOR
        }
        type Judge implements Node {
          name: String
          caption: String
          photo: Image
          position: Position,
          nominations: [Nomination]
        }`);

    let judges = addCollection("Judge");
    const judgesRaw = JSON.parse(fs.readFileSync("./data/judges.json"));
    for (const key in judgesRaw) {
      const judge = judgesRaw[key];
      if (!judge.hidden)
        judges.addNode(
          fixImage(
            {
              id: key,
              name: judge.name,
              caption: judge.caption,
              photo: judge.photo,
              position: judge.position,
              nominations: judge.nominations.map((nominationId) =>
                createReference("Nomination", nominationId)
              ),
            },
            "photo"
          )
        );
    }
  });

  api.createPages(() => {
    // Use the Pages API here: https://gridsome.org/docs/pages-api/
  });
};
