module.exports = {
    files: ["./assets/svg/icons/*.svg"],
    fontName: "itelent",
    classPrefix: "ite-",
    baseSelector: ".ite",
    types: ["eot", "woff2"],
    fixedWidth: true,
};
