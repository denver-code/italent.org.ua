// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from "~/layouts/Default.vue";
import LazyYoutubeVideo from "vue-lazy-youtube-video";

import "./icons.ts";

export default function (Vue, { router, head, isClient }) {
    Vue.component("Layout", DefaultLayout);
    Vue.component("LazyYoutubeVideo", LazyYoutubeVideo);

    router.options.scrollBehavior = function (to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        }
        if (to.hash) {
            return { selector: to.hash };
        }

        document.getElementById("app").scrollIntoView();
    };

    router.afterEach((to, from) => {
        if (to.hash && to.hash != "#undefined") {
            var hash = to.hash;
            setTimeout(
                () => document.getElementById(hash.slice(1))?.scrollIntoView(),
                0
            );
            window.location.hash = "#undefined";
            setTimeout(
                () =>
                    history.replaceState(
                        "",
                        null,
                        window.location.href.split("#")[0] + hash
                    ),
                100
            );
        }
    }),
        (head.htmlAttrs = { lang: "uk" });
    head.link.push({
        rel: "stylesheet",
        href:
            "/assets/font/fonts.css",
    });
}
