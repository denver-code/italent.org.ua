const { merge } = require('webpack-merge')
const GoogleFontsPlugin = require("@beyonk/google-fonts-webpack-plugin")

module.exports = {
  siteName: 'iTalent',
  siteDescription: 'Всеукраїнський конкурс з IT та кіберспорту серед молоді.',
  titleTemplate: '%s | iTalent всеукраїнський конкурс з IT та кіберспорту',
  icon: './src/favicon.png',
  siteUrl: 'https://italent.org.ua',
  css: {
    loaderOptions: {
      scss: {
        additionalData: `@import "~/assets/scss/utility.scss";`
      }
    }
  },
  templates: {
    Nomination: '/nomination/:slug'
  },
  chainWebpack: config => {
    config.module
        .rule('css')
        .oneOf('normal')
        .use('postcss-loader')
        .tap(options => {
          options.plugins.unshift(...[
            require('postcss-import'),
            require('postcss-nested'),
          ])

          if (process.env.NODE_ENV === 'production') {
          // if(true){
            options.plugins.push(...[
              require('@fullhuman/postcss-purgecss')({
                content: [
                  'src/assets/scss/*.scss',
                  'src/**/*.vue',
                  'src/**/*.js'
                ],
                extractors: [
                  {
                    extractor: (content) => {
                      return content.match(/[A-Za-z0-9-_:\/]+/g) || []
                    },
                    extensions: ['css', 'vue', 'js']
                  }
                ],
                whitelist: ['svg-inline--fa'],
                whitelistPatterns: [/shiki/, /ico-$/]
              })
            ])
          }
          return options
        })
    const types = ['vue-modules', 'vue', 'normal-modules', 'normal']

    config.module
        .rule('js')
        .exclude
        .add(/icons\.ts$/)
        .end()
    config.module
        .rule('font-icons')
        .test(/icons\.ts$/)
        .use('css-extract')
        .loader(
            'vue-style-loader'
        )
        .end()
        .use('css-loader')
        .loader('css-loader')
        .end()
        .use('webfonts-loader')
        .loader('webfonts-loader')
        .end()
  },
  configureWebpack(config) {
    return merge({
      plugins: [
        new GoogleFontsPlugin({
          filename: 'assets/font/fonts.css',
          fonts: [
            { family: "Montserrat", variants: [ "100", "200", "300", "400", "500", "600", "700", "800", "900" ], subsets: ['cyrillic', 'cyrillic-ext'] }
          ]
        })
      ]
    }, config)
  },
  plugins: [
    {
      use: '@gridsome/plugin-sitemap',
      options: {
        exclude: [],
        config: {
          '/nomination/*': {
            changefreq: 'weekly',
            priority: 0.5,
            lastmod: '2020-09-21',
          },
          '/*': {
            changefreq: 'monthly',
            priority: 0.2,
            lastmod: '2020-09-21',
          }
        }
      }
    },
    {
      use: 'gridsome-plugin-gtag',
      options: {
        id: 'UA-107920964-1'
      }
    }
  ]
}
