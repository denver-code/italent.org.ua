# iTalent Main newgen

Based on [grdisome](https://gridsome.org)

### 1. Clone this project

```
git clone git@gitlab.com:aquaminer/italent.org.ua.git italent-main && cd italent-main
```

### 2. Install depends

```
npm i
#OR
yarn
```

### 3. Run in development mode

```
npm run develop
#OR
yarn develop
```
